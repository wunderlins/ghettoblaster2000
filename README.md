# GhetoBlaster2000

keeps your projects and your head cool.

![Finished Project Image](docs/IMG_20200923_223101.jpg)

## BOM

- The cheapest lamp I could find: [Ikea Tertial](https://www.ikea.com/ch/en/p/tertial-work-lamp-dark-grey-50355395/)
- STM32F411 [blue pill](docs/Blackpill_Pinout.png) dev board -- overkill, but I had them laying around
- Alps Rotary encoder [EC11](docs/alps_alps-s-a0008379064-1-1733314.pdf)
- Some spare fan from an old PC power supply: [ADDA AD0912UX](docs/ADDA_AD0912UX_A7BGL_C.pdf)
- [LM78S05](docs/LM78S05.pdf) 5V regulator
- PETG 3D Printed [Fan mount](cad/)