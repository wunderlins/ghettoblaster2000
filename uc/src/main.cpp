#include "Arduino.h"

// range, set max minValue for the value
const int maxValue = 255;
const int minValue = 0;

const byte pwmPin       = PB5;
const byte encoder0PinA = PB8;
const byte encoder0PinB = PB9;
const byte buttonPin    = PB7;
const byte ledPin       = PC13;

volatile int encoder0Pos = 0;
volatile bool buttonPushed = false;

/*
void doEncoderA() {
  // look for a low-to-high on channel A
  if (digitalRead(encoder0PinA) == HIGH) {

    // check channel B to see which way encoder is turning
    if (digitalRead(encoder0PinB) == LOW) {
      if (encoder0Pos < maxValue)
        encoder0Pos++;          // CW
    }
    else if (encoder0Pos > minValue) {
      encoder0Pos--;         // CCW
    }
  }

  else   // must be a high-to-low edge on channel A
  {
    // check channel B to see which way encoder is turning
    if (digitalRead(encoder0PinB) == HIGH) {
      if (encoder0Pos < maxValue) encoder0Pos++; // CW
    }
    else if (encoder0Pos > minValue)  {
      encoder0Pos--;          // CCW
    }
  }
  //Serial.println (encoder0Pos, DEC);
  // use for debugging - remember to comment out
}

void doEncoderB() {
    byte Pin1 = digitalRead(encoder0PinA);
    byte Pin2 = digitalRead(encoder0PinB);

    // look for a low-to-high on channel B
    if (Pin2 == HIGH) {
        // check channel A to see which way encoder is turning
        if (Pin1 == HIGH) {
            if (encoder0Pos < maxValue) encoder0Pos++; // CW
        } else if (encoder0Pos > minValue) {
            encoder0Pos--;         // CCW
        }
    }

    // Look for a high-to-low on channel B
    else {
        // check channel B to see which way encoder is turning
        if (Pin1 == LOW) {
            if (encoder0Pos < maxValue)
                encoder0Pos++;          // CW
        }
        else if (encoder0Pos > minValue) {
            encoder0Pos--;          // CCW
        }
    }
}
*/

void isrEncoder(const byte Pin1, const byte Pin2, const byte state) {
    // look for a low-to-high on channel B
    if (Pin2 == HIGH) {
        // check channel A to see which way encoder is turning
        if (Pin1 == state) {
            if (encoder0Pos < maxValue) encoder0Pos++; // CW
        } else if (encoder0Pos > minValue) {
            encoder0Pos--;         // CCW
        }
    }

    // Look for a high-to-low on channel B
    else {
        // check channel B to see which way encoder is turning
        if (Pin1 != state) {
            if (encoder0Pos < maxValue)
                encoder0Pos++;          // CW
        }
        else if (encoder0Pos > minValue) {
            encoder0Pos--;          // CCW
        }
    }
}

// CCW
void isrEncoder1() {
    isrEncoder(digitalRead(encoder0PinA), digitalRead(encoder0PinB), LOW);
}

// CW
void isrEncoder2() {
    isrEncoder(digitalRead(encoder0PinB), digitalRead(encoder0PinA), HIGH);
}

void buttonEvent() {
    if (digitalRead(buttonPin) == LOW) {
        buttonPushed = false;
        digitalWrite(ledPin, !LOW);
        encoder0Pos = 0;
    } else {
        buttonPushed = true;
        digitalWrite(ledPin, !HIGH);
    }
}


void setup() {
    pinMode(encoder0PinA, INPUT);
    pinMode(encoder0PinB, INPUT);
    pinMode(buttonPin, INPUT);
    pinMode(ledPin, OUTPUT);
    pinMode(pwmPin, OUTPUT);
    delay(100);
    analogWrite(pwmPin, 0);

    // encoder pin on interrupt 0 (pin 2)
    pinMode(encoder0PinA, INPUT_PULLDOWN);
    attachInterrupt(digitalPinToInterrupt(encoder0PinA), isrEncoder1, CHANGE);

    // encoder pin on interrupt 1 (pin 3)
    pinMode(encoder0PinB, INPUT_PULLDOWN);
    attachInterrupt(digitalPinToInterrupt(encoder0PinB), isrEncoder2, CHANGE);

    // button interrupt
    pinMode(buttonPin, INPUT_PULLDOWN);
    attachInterrupt(digitalPinToInterrupt(buttonPin), buttonEvent, CHANGE);

    // initial state
    digitalWrite(ledPin, !LOW);

    Serial.begin (115200);
}

void loop() {
    Serial.printf("pos: %d, button: %d\n", encoder0Pos, buttonPushed);
    delay(200);

    if (encoder0Pos)
        analogWrite(pwmPin, map(encoder0Pos, 1, 255, 77, 255));
    else
        analogWrite(pwmPin, 0);

}
